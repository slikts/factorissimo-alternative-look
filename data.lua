local prefix = "__Factorissimo_Alternative__/graphics/variant-"
  .. (settings.startup["factorissimo-alternative-variant"].value and 2 or 1)

local function graphic(name)
  return prefix .. "/" .. name .. ".png"
end

local function update(type)
  local storage = data.raw[type]

  for k,v in pairs(storage) do
    local n = k:match "factory%-(%d+)"

    if (n) then
      local name = "factory-" .. n
      local layers = v.pictures and v.pictures.picture.layers or v.picture.layers
      layers[1].filename = graphic(name .. '-shadow')
      layers[2].filename = graphic(name)
    end
  end
end

update("storage-tank")
update("simple-entity")
